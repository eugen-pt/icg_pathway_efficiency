# -*- coding: utf-8 -*-
"""
Created on Tue Aug  5 19:10:52 2014

Just a test of PCA approach to histogram data

@author: ep
"""


x = plt.mlab.PCA(allEffs_distrs)

figure()
for j in range(x.numcols-1):
  for i in range(x.numcols-1):
    subplot(x.numcols,x.numcols,1+j+i*x.numcols)
    if(j==i):
      hist(x.Y[:,j])          
    else:
      plot(x.Y[:,j],x.Y[:,i],'.')

"""

can't say that I see any sort of clustering here =(

"""

""" """
#
