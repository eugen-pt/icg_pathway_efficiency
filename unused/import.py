# -*- coding: utf-8 -*-
"""
Created on Fri Aug  1 10:03:16 2014

@author: EP
"""

import urllib2
import pickle
import re

with open('links.txt') as f:
  Links = f.readlines()

Links = [s[0:s.index('/kgml')] for s in Links]

Ls = []

try:
  with open('LinksDone.txt') as f:
    LinksDone = [s[0:-2] for s in f.readlines()]
  with open('Names.txt') as f:
    Names = [s[0:-2] for s in f.readlines()]
  with open('Classes.txt') as f:
    Classes = [s[0:-2].split(';') for s in f.readlines()]
  with open('Genes.txt') as f:
    Genes = [s[0:-2].split(';') for s in f.readlines()]  
  if(len(unique(map(len,[LinksDone,Names,Classes,Genes])))>1):
      raise ValueError('Different length of read arrays!')
except:  
  LinksDone = []
  Names = []
  Classes = []
  Genes = []
  with open('LinksDone.txt','w') as f:
    pass
  with open('Names.txt','w') as f:
    pass
  with open('Classes.txt','w') as f:
    pass
  with open('Genes.txt','w') as f:
    pass

import threading


for j in range(len(Links)):
  
  url = Links[j]  

  if(url in LinksDone):
    continue


  print('%i/%i..'%(j,len(Links)))
  
  # Loading
  R=urllib2.urlopen(url)
  rr=R.read()
  L=rr.split('\n')
  
#  Ls.append(L) #for debug purposes

  name = L[1][4:L[1].index(' - ')].strip()
  Names.append(name)

  # Classes - we search for a line with 'CLASS' in it (for all of them actually)
  classes = []
  for l in L:
    try:
      classes.extend([s.strip() for s in l[(l.index('CLASS')+5):].strip().split(';')])
    except:
      pass
  Classes.append(classes)
  # Genes - 
  genes=[]
  for j in range(len(L)):
      if('GENE' in L[j]):
          for i in range(j,len(L)):
              if(('COMPOUND' in L[i]) or ('REFERENCE' in L[i])):
                  break
              temp = re.findall('^(GENE)? +[0-9]+ +([A-Za-z0-9]+);',L[i])
              try:
                genes.append(temp[0][1])
              except:
                print('Some issue in:\nurl='+url+'\nline='+str(i))
                      
          break
  Genes.append(genes)    

  LinksDone.append(url)
  with open('LinksDone.txt','a') as f:
    f.write(url+'\r\n')
  with open('Names.txt','a') as f:
    f.write(name+'\r\n')
  with open('Classes.txt','a') as f:
    f.write(';'.join(classes)+'\r\n')
  with open('Genes.txt','a') as f:
    f.write(';'.join(genes)+'\r\n')


# Pickle dump..
with open('LinksNamesClassesL.pickle','w') as f:
  pickle.dump((LinksDone,Names,Classes,Genes,Ls),f)  
 
 
# Matrix of classes 
allClasses = [] 
[allClasses.extend(c) for c in Classes]
  
aC=allClasses  
  
uaC  = unique(aC)

Mx = []
for c in uaC:
  Mx.append([c in A for A in Classes])  

sMx = map(sum,Mx)

tix = argsort(sMx).tolist()
tix.reverse()

uaC = array(uaC)
Mx = array(Mx)

uaC = uaC[tix]
Mx = Mx[tix,:]
  
""" Saving the matrix..  """

save=0  
if(save):
  with open('Classes_table.csv','w') as f:
    f.write('links,')
    for c in uaC:
      f.write(c+',')
    f.write('\n')
    for j in range(len(LinksDone)):
      l=LinksDone[j]
      f.write(l+',')
      for c in uaC:
        f.write(str(int(c in Classes[j]))+',')
      f.write('\n')

""" Calculating smart stuff """

classMx = Mx

classInt = np.zeros(shape=(len(Classes)))
  
maxClass = max(find(sum(Mx,axis=1)>20))
tempMul = 10**(maxClass -array(range(maxClass)))

for j in range(len(Classes)):
    classInt[j] = sum(Mx[0:maxClass,j]*tempMul)


  
#