# -*- coding: utf-8 -*-
"""
Created on Tue Jun 17 14:21:15 2014

@author: ep_work
"""

from scipy.stats import fisher_exact
from scipy import stats

from ep_utilities import *
#from ep_bioinfo_load105 import *

class BinGO_output:
  fpath=''
  GOs_ids = []
  GOs_terms = []
  GOs_NHs = []
  GOs_TotalGenes = 0
  GOs_GOTotalGenes = 0
  
with open('all_s_ProtNames.bgo') as f:
#with open('all_s_ProtNames_Binomial.bgo')  as f: 
    LSa = f.readlines();
    
LSa = LSa[25:]    

LSa = [L[0:-1].split('\t') for L in LSa]

# header = LSa[0]
LSa=array(LSa[1:])

GOs_ids = array(map(int,LSa[:,0]))
GOs_terms = LSa[:,7]


GOs_NHs = map(int,LSa[:,3])
GOs_NTs = map(int,LSa[:,4])
GOs_TotalGenes = int(LSa[0,5])
GOs_GOTotalGenes = int(LSa[0,6])

if(0):  #if you want to test GO's p-value calcultaion
    GOs_ps = array(map(float,LSa[:,1]))
    GOs_pcs = array(map(float,LSa[:,2]))
    print('Calculating my p-values')
    
    GOs_statsPs = GOs_ps*0.0
    GOs_statsHPs = GOs_ps*0.0
    for j in range(len(GOs_ids)):
        GOs_statsPs[j] = ep_GOfisher_exact(GOs_NHs[j],GOs_NTs[j],GOs_TotalGenes,GOs_GOTotalGenes)
    #    GOs_statsHPs[j] = stats.hypergeom.sf(GOs_NHs[j],GOs_TotalGenes,GOs_NTs[j],1329)

print('parsing ProtNames')    
GOs_ProtNames = [s.split('|') for s in LSa[:,8]]

GOs_allProtNames = []
[GOs_allProtNames.extend(a) for a in GOs_ProtNames]
GOs_allProtNames = unique(GOs_allProtNames).tolist()


GOs_sProtIxs = [array( [GOs_allProtNames.index(s) for s in A]) for A in GOs_ProtNames]

print('Inverse mapping:  proteins->GOixs')
try:
    s_PGOixs
    print('  already in memory.')
except:
    s_PGOixs = [array(find([(j in A) for A in GOs_sProtIxs])) for j in range(s_NP)];

GOs_PCounts = array(map(len,GOs_sProtIxs))
print('Finished')