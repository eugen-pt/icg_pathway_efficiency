# -*- coding: utf-8 -*-
"""
Created on Fri Jun 20 19:30:29 2014

@author: ep_work
"""



import random
import numpy as np


from ep_utilities import *
from ep_bioinfo_load105 import *
from ep_bioinfo_2014_06_17B_binGO_resultsParser import *


#
#       OverRepresentation
#
#

def ep_saveDict(Dict,fname,listname,keys=[],titles=[],AddData=[]):
    from openpyxl import Workbook, load_workbook
    try:
        wb = load_workbook(fname)
    except:
        wb = Workbook() 
    ws1 = wb.create_sheet(title = listname)
    
    if(len(keys)==0):
        keys = Dict.keys()
    if(len(titles)==0):
        titles = keys
    
    for j in range(len(keys)):
        ws1.cell(row=0,column=j).value=titles[j]
        for i in range(len(Dict[keys[j]])):
            ws1.cell(row=i+1,column=j).value=Dict[keys[j]][i]
    
    if(len(AddData)>0):
        for key in AddData.keys():
            ws2 = wb.create_sheet(title = key)
            for j in range(len(AddData[key])):
                for i in range(len(AddData[key][j])):
                    ws2.cell(row=i,column=j).value=AddData[key][j][i]
        
    wb.save(fname)
    
def ep_BinGO_save(R,fname='temp.xlsx',listname='ep_BinGo_outputt',AddData=[]):
    
    ep_saveDict(R,fname,listname,keys = ['GOids','GOterms','p','FDRp','NH','HNT','NT','js_ProtNames'],titles = ['GO ID','Go term','p unc.','FDR p','cluster N','cluster Total','total N','SwissProt IDs'],AddData=AddData)
    
def tempAAA():    
    from openpyxl import Workbook, load_workbook
    try:
        wb = load_workbook(fname)
    except:
        wb = Workbook() 
    ws1 = wb.create_sheet(title = listname)
    ws1.cell(row=0,column=0).value='GO ID'
    ws1.cell(row=0,column=1).value='GO term'
    ws1.cell(row=0,column=2).value='p unc.'
    ws1.cell(row=0,column=3).value='FDR p'
    ws1.cell(row=0,column=4).value='cluster N'
    ws1.cell(row=0,column=5).value='total N'
    ws1.cell(row=0,column=6).value='SwissProt IDs'
    for j in range(len(R['p'])):
        ws1.cell(row=j+1,column=0).value=R['GOids'][j]
        ws1.cell(row=j+1,column=1).value=R['GOterms'][j]
        ws1.cell(row=j+1,column=2).value=R['p'][j]
        ws1.cell(row=j+1,column=3).value=R['FDRp'][j]
        ws1.cell(row=j+1,column=4).value=R['NH'][j]
        ws1.cell(row=j+1,column=5).value=R['NT'][j]
        ws1.cell(row=j+1,column=6).value=' '.join(R['s_ProtNames'][j])
    wb.save(fname)    
    

def ep_BinGO(s_Pixs,p=0.05,fname='',AddData=[]):
    s_Pixs = unique(s_Pixs)
    src_Pixs = s_Pixs
    R={}
    keys = ['p','FDRp','GOids','GOterms','GOixs','js_ProtNames','s_Pixs','NH','NT','HNT']
    for key in keys:
        R[key]=[]
    
    hGOixs = [s_PGOixs[j] for j in s_Pixs]
    
    # Discard proteins without annotations
    tix = find(array(map(len,hGOixs))>0)
    
    if(len(tix)>0):
        s_Pixs = array(s_Pixs)[tix]
        hGOixs = array(hGOixs)[tix]    
            
        
        allGOixs = unique(hstack(hGOixs))
        
        for ix in allGOixs:
            hix = array([ix in A for A in hGOixs])
            NH = sum(hix)
    #        print(NH)
    #        print(len(s_Pixs))
    #        print(GOs_NTs[ix])
            R['p'].append(ep_GOfisher_exact(NH,len(s_Pixs),GOs_NTs[ix]))
            R['GOterms'].append(GOs_terms[ix])
            R['GOids'].append(GOs_ids[ix])
            R['GOixs'].append(ix)
            R['js_ProtNames'].append(' '.join(s_ProtNames[s_Pixs[hix]]))
            R['s_Pixs'].append(s_Pixs[hix])
            R['NH'].append(NH)
            R['NT'].append(GOs_NTs[ix])
            R['HNT'].append(len(s_Pixs))
        R['FDRp'] = pFDRadj(R['p'])
        
#        print('__len(R[p])')
#        print(len(R['p']))
#        print(len(R['FDRp']))
        
        tix = array(find(array(R['FDRp'])<p))
#        print(len(tix))
        for key in keys:
            R[key]=array(R[key])[tix]
        
        if(len(tix)>0):
#            print('len(R[p])')
#            print(len(R['p']))
#            print(len(R['FDRp']))
#            print(len(R['p']))
            tix = argsort(R['p'])
#            print(len(tix))
            for key in keys:
                R[key]=array(R[key])[tix]
            
            
    if(len(fname)>0):
        print('   saving..')
        if(len(fname)==2):
            listname=fname[1]
            fname=fname[0]
        else:
            listname='ep_BinGo_outputt'
            
        ProteinList = ['All SwissProt IDs:']
        [ProteinList.append(s_ProtNames[A]) for A in src_Pixs]
        AddData[listname+'_list'] = [ProteinList]
        ep_BinGO_save(R,fname=fname,listname=listname,AddData=AddData)
        
    return R
    
# returns number of links pesent in StringLinks array of dictionaries, 
#   where keys are numbers of linked nodes and values are to be compared to threshold
def ep_bioinfo_NLinks(nums,LinksDicts,Max,thresh=400,givetotal=0,mode=0):
    NT = 0;
    NH = 0;
    
    NT = len(nums)*(len(nums)-1)/2
    if(type(LinksDicts[0])!=type({})):
        X = LinksDicts[nums,:][:,nums]
        X = X[np.triu_indices(len(nums))]
        #NH = sum(X>thresh)
        NH = len(X[X>thresh])
    else:
        if(mode==0):
            for i1 in range(len(nums)):
                for i2 in range(i1+1,len(nums)):
                    if(nums[i2] in LinksDicts[nums[i1]].keys()) and (LinksDicts[nums[i1]][nums[i2]]>thresh):
                        NH=NH+1
        elif(mode==1):
            for i1 in range(len(nums)):
                for i2 in range(i1+1,len(nums)):
                    try:                    
                        NH=NH+(LinksDicts[nums[i1]][nums[i2]]>thresh)
                    except:
                        pass
    if(givetotal):
        return NH,NT            
    return NH            
        
        

def ep_bioinfo_RandomNLinks(NNodes,LinksDicts,Max,thresh=400,nrt = 100,new=0,mode=0):
    random.seed();
    if(new):
        X = np.zeros([Max,Max])
        for j in range(len(LinksDicts)):
            k = LinksDicts[j].keys()
            if(len(k)>0):
                X[np.array(k),j] = LinksDicts[j].values()
        return    [ep_bioinfo_NLinks(random.sample(range(Max),NNodes),X,Max,thresh=thresh,mode=mode) for rj in range(nrt)]

    else:
        return    [ep_bioinfo_NLinks(random.sample(range(Max),NNodes),LinksDicts,Max,thresh=thresh,mode=mode) for rj in range(nrt)]
        
#        