

%% matrix

[a,b,c]=xlsread('efficiency_all_pathways_average.xlsx','matrix');

M.links = b(2:end,1);
M.mx = a;

%% efficiencies

[a,b,c]=xlsread('efficiency_all_pathways_average.xlsx','efficiency_all_pathways');

E.links = b(1:end-1,1);
E.avgs = a(:,1);

ix = find(~isnan(E.avgs));
a=a(ix,:);
E.avgs = E.avgs(ix);
E.links = E.links(ix);

tmeans = zeros(length(E.links),1);
E.effs = {};
E.means = [];
for j=1:length(E.links)
  s = E.links{j};
  E.links{j} = s(strfind(s,'path:')+5:strfind(s,'/kgml')-1);
  E.effs{j} = a(j,2:find(isnan(a(j,:)))-1);
  tmeans(j) = mean(E.effs{j});
  E.means(j) = mean(E.effs{j});
end;

alleffs = [];
for j=1:length(E.links)
  alleffs=[alleffs;E.effs{j}(:)];
end;

save('temp.mat','E','M','alleffs')





