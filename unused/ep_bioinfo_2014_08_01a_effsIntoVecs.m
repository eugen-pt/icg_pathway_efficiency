
function ep_bioinfo_2014_08_01a_effsIntoVecs

load('temp.mat')

Nbins = 10;

effEdges = [0];
for j=1:Nbins-1
  effEdges=[effEdges,prctile(alleffs,100*j/Nbins)];
%   effEdges=[effEdges,percentile(alleffs,100*j/Nbins)];
end;
effEdges=[effEdges,max(alleffs)];


effVecs = [];
for j=1:length(E.links)
  temp=histc(E.effs{j},effEdges);
  effVecs=[effVecs;temp(1:end-1)];
end;


function x=percentile(Arr,p)
  try
%    AAA
    x = prctile(Arr,p);
  catch
    tx = [min(Arr):((max(Arr)-min(Arr))/1000):max(Arr)];
    minx = tx(1);
    minpdif = abs(sum(Arr<tx(1))*100/length(Arr)-p);
    for j=2:length(tx)
      pdif = abs(sum(Arr<tx(j))*100/length(Arr) - p);
      if(pdif<minpdif)
        minpdif=pdif;
        minx=tx(j);
      end;
    end;
    x=minx;
  end;