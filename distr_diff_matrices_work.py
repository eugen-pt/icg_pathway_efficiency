# -*- coding: utf-8 -*-
"""
Created on Mon Aug 04 17:29:25 2014

@author: ep_work
"""

from url_pathway_info_import import *

import random

""" Sorting the Links and Stuff: """

tix = argsort(LinksDone)
LinksDone = array(LinksDone)[tix]
hsaIDs = array([s[s.index(':hsa')+4:] for s in LinksDone])
Names = array(Names)[tix]
Classes = array(Classes)[tix]
Genes = array(Genes)[tix]

""" Checking if it fits the proposed matr order stored in Url_list.txt """

with open('src\\Url_list.txt') as f:
  ls = f.readlines()

matr_hsaIDs = array([s[s.index(':hsa')+4:s.index('/kgml')] for s in ls[0:-1]])
 #-1 for one extra pathway - dunno why

# if orders of links (pathways) do not match
if(sum(matr_hsaIDs!=hsaIDs)>0):
  raise ValueError('Im too lazy to correct the orders but they dont fit so do it now or be forever mistaken!')
# but they usually do


def imagesc(X):
  plt.imshow(X,interpolation='nearest')


""" loading matr """
try:
  matr
except:
  print('loading matr..')
  with open('src\\matrix.pkl') as f:
    matr = pickle.load(f)
    
# Discarding the tupled values of the algorithm
mx = np.zeros(shape=(len(matr),len(matr)))
for j in range(len(matr)):
  for i in range(len(matr)):
    try:
      mx[j][i]=matr[j][i][1]
    except:
      mx[j][i]=matr[j][i]

""" Discarding all-zero elements: """

# indices of valid ones:
nzix = find(sum(mx>0,axis=1)>0)

# applying indices to all that matters:
nzmx = array([a[nzix] for a in mx[nzix]])
nzcMx = array([a[nzix] for a in cMx])
nzhsaIDs = hsaIDs[nzix]
nzNames = Names[nzix]
nzClasses = Classes[nzix]
nzGenes = Genes[nzix]

# looking for unique Classes, getting matrix of classes,
#   result's already sorted so that popular classes go first
nzClassNames,nzcMx,cInt = ClassesPostProc(nzClasses)


try:
  uaG
  gMx
  gInt
  # there are a lot of genes so I put this inside a try box not to recalculate
except:
  # looking for unique Genes - processing is basically the same as for the Classes
  uaG,gMx,gInt = ClassesPostProc(nzGenes)

""" saving allGenes """
if(0):
  with open('src\\allGenes.txt','w') as f:
    for g in uaG:
      f.write(g+'\r\n')  

""" plotting  with sorted classes ( just for fun )"""

if(0):
  tix = argsort(cInt)
  
  temp = [[0]*len(tix)]
  temp.extend( nzcMx[:,tix].tolist())
  temp.extend([a[tix] for a in nzmx[tix]])
  
  imagesc(temp)
  
#  You should see some sort of a pattern here, but not the clustered one
  # But you should see what does the "cluster image" look like (upper red/blue part)
    # - simple red lines corresponding to major classes staying together
""" hierarchical clustering """


from scipy.cluster.hierarchy import *#linkage,leaves_list #dendrogram
# just to write it once here and only use a fuction everywhere else
def get_distVec(x):
  distVec = []
  for j in range(shape(x)[0]):
    for i in range(j+1,shape(x)[0]):
      distVec.append(x[j][i])
  return array(distVec)

""" getting distance vector """

distVec = get_distVec(nzmx)

distVec[distVec==0] = min(distVec[distVec>0])
distVec = 1-distVec  #1-p-value
#distVec = -log10(distVec)  #-log10(p-value)
#distVec = -log(distVec)  #-log(p-value)


""" linkage and stuff """

L = linkage(distVec)
L = linkage(distVec,'average')
L = linkage(distVec,'complete')


x=fcluster(L,7,criterion='maxclust')#1-p-value #'average'
x=fcluster(L,10,criterion='maxclust')#-log(p-value) #'average'
x=fcluster(L,4,criterion='maxclust')#-log(distVec) #'complete'
x=fcluster(L,10,criterion='maxclust')#1-p-value #'average'
x=fcluster(L,10,criterion='maxclust')#-log10(p-value) #'complete'
x=fcluster(L,25,criterion='maxclust')#1-p-value #'complete'

nzClusters = array(x)

""" drawing clusters """
if(1):
  
  figure()
  subplot(1,2,1)
  d  = dendrogram(L)
  ix = d['leaves']
  
  tix = ix
  
  temp = [[0]*len(tix)]
  temp.extend( nzcMx[:,tix].tolist())
  temp.extend([a[tix] for a in nzmx[tix]])
  
    
  if(1): #Coloring clusters
    temp = array(temp)
    tempm = max(temp.flatten())# "coloring" constant

    # coloring as clusters (fun!!)
    x=x[tix]
    ux = unique(x)

    if(len(ux)>1):
      rj=argsort([np.where(x==i)[0][0] for i in ux])
      trj = list(rj[range(0,len(rj),2)])
      trj.extend(rj[range(1,len(rj),2)])
      rj=trj
      rj=[rj.index(i) for i in range(len(ux))]
    else:
      rj = random.sample(ux,len(ux))  # I randomize clusters to make image less smooth 
    # (if consequent groups of pathways correspond to consequent clusters 
    #   - we see similar colors, sometimes indistinguishable)
    for uxj in range(len(ux)):
      temp[:,find(x==ux[uxj])]=temp[:,find(x==ux[uxj])]+tempm*rj[uxj]/(max(rj))
  
  subplot(1,2,2)
  imagesc(temp)

else:
  #  we can only find leaves 
  ix = leaves_list(L) # in case we dont want to draw a dendrogram 


""" we find big clusters """

uc = unique(nzClusters)
temp = array([sum(nzClusters==j) for j in uc])
tix = argsort(temp).tolist()
tix.reverse()

tix = array(tix)[find(temp[tix]>10)]

nzBigClusters = nzClusters*0 - 1
for j in range(len(tix)):
  nzBigClusters[array(nzClusters)==uc[tix[j]]] = j

"""
 making up names for the big Clusters

also I don't like the erst being with the lowest value and therefore the first among the unique elements

 """

nzBigClusterNames=[]
nzBigClusterNames.extend(['Cluster_'+str(x) for x in range(1,max(nzBigClusters)+2)])
nzBigClusterNames.append('Rest')
nzBigClusters[nzBigClusters==-1]=max(nzBigClusters)+1


""" """
#