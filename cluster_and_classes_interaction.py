# -*- coding: utf-8 -*-
"""
Created on Tue Aug  5 15:54:51 2014


!!! requires distr_diff_matrices_work.py to have been launched
      (uses nz* stuff)

uses nzBigClusters (and nzBigClusterNames) and Classes

to build correspondence matrix, i.e. how many pathways of class A are in cluster #N

TODO:
- attach a proper statisticl test

@author: ep
"""


try:
  nzBigClusters
except:
  raise ValueError('launch distr_diff_matrices_work.py first and read the head of the files!') #the only Error type I remember at the moment )

try:
  clusterSrcS
except:
  clusterSrcS=''


""" """

table = np.zeros(shape=(len(nzBigClusterNames)+1,len(nzClassNames)))
for cj in range(len(nzClassNames)):
  table[0,cj] = sum(nzcMx[cj,:])
for bcj in range(len(nzBigClusterNames)):
  for cj in range(len(nzClassNames)):
    table[1+bcj,cj] = sum(nzcMx[cj,nzBigClusters==bcj])
  
outTable = [['Class name:','Total pathways:']]
outTable[0].extend(nzBigClusterNames)
for cj in range(len(nzClassNames)):
  temp = [nzClassNames[cj]]
  temp.extend(map(str,table[:,cj].transpose().tolist()))
  outTable.append(temp)


with open('output\\clusterClasses_table.csv','w') as f:  
  for a in outTable:
    f.write(','.join(a)+'\r\n')
""" """
#
