Changed buggy version of all categories bingo output via GVIM and commands :
(for BiolProc)
:%s/\([0-9]\)6181^I/\1^I6181^I
:%s/^I^I/^I

where ^I corresponds to me pressing <Tab>  (for some reason \t didn't work here)

The first line adds a missing \t before total gene number
and second line squashes two consequtive \t's that are placed before first text field

for other files - just change 6181 to full gene number (which is the same in all lines)
