# -*- coding: utf-8 -*-
"""
Created on Tue Aug  5 17:01:20 2014

Generates text files:

allGenes_nz.txt
allGenes.txt
allGenes_not_in_nz.txt

all pretty much self-explanatory

@author: ep
"""

def getAllElts(arr):
  R = []
  [R.extend(a) for a in arr]
  return unique(R)

print('writing allGenes_nz..')
with open('output\\allGenes_nz.txt','w') as f:
    [f.write(s+'\r\n') for s in getAllElts(nzGenes)]


print('writing allGenes..')
with open('output\\allGenes.txt','w') as f:
    [f.write(s+'\r\n') for s in getAllElts(Genes)]

print('writing allGenes_not_in_nz..')
allGenes = getAllElts(Genes)
allnzGenes = getAllElts(nzGenes)
with open('output\\allGenes_not_in_nz.txt','w') as f:
    for c in allGenes:
        if(c not in allnzGenes):
            f.write(c+'\r\n')

""" """
#
print('finished.')
