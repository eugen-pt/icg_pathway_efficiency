# -*- coding: utf-8 -*-
"""
Created on Tue Aug  5 09:27:31 2014


- Loads efficiencies vectors
- Groups them per class
- Plots mean effs distributions per class
- plots distributions of distributions per class


@author: ep
"""

import csv
from url_pathway_info_import import *

import numpy as np
import matplotlib.pyplot as plt
## Spline mode
#from scipy import interpolate
from scipy.ndimage import gaussian_filter1d

def splot(x,y,k=10):
## Spline mode
#  tck = interpolate.splrep(x, y, s=0)
#  xnew = np.arange(x[0],x[-1],(x[1]-x[0])/k)
#  plot(xnew,interpolate.splev(xnew, tck, der=0))
## Gaussian mode
  xnew = np.arange(x[0],x[-1],(x[1]-x[0])/k)
  ynew = np.interp(xnew,x,y)
  ynew = gaussian_filter1d(ynew,k/2)
  plot(xnew,ynew)

def csvread(fpath,delim=';'):
  with open(fpath, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=delim)#, quotechar='|'
    R = [row for row in spamreader]
  return R


""" Global flags """

G_drawAll = 1


""" Loading and basic preprocessing """

src_Effs = array([ [float(s.strip()) for s in a] for a in csvread('src\\efficiency_all_pathways.csv') ])
Effs=src_Effs
allEffs = []
[allEffs.extend(a) for a in Effs];
allEffs = array(allEffs)
# ordered as matr_hsaIDs

mEffs = array(map(mean,Effs))
lEffs = array(map(len,Effs))


# select only "OK" pathways

tix = find(lEffs>30) # >30 corresponds to nz* indices

Effs = Effs[tix]
mEffs = mEffs[tix]
lEffs=lEffs[tix]

hcMx = cMx[:,tix]
hhsaIDs = array(hsaIDs)[tix]
hClasses = Classes[tix]
hNames = Names[tix]

""" just checking with another file.. """
"""
tEffs_src = csvread('src\\efficiency_all_pathways_average_Saved_from_xlsx.csv',delim=',')[0:-1]
tEff_hsaIDs = [a[0][a[0].index(':hsa')+4:a[0].index('/kgml')] for a in tEffs_src]
[a.append('') for a in tEffs_src]  # so that there's always an empty element
tEffs = [map(float,a[2:a.index('')]) for a in tEffs_src]
mtEffs = map(mean,tEffs)
ltEffs = map(len,tEffs)
"""

"""
Now I want to find class-related clusters

1. we find maximum number of classes that a pathway belongs to = max(sum(cMx[0:j,:],axis=0))
    among the first j classes (ordered by total pathway numbers = by "popularity")
     and we do this for all js

      we want to know, how many classes do not overlap
        so we find the last time (-1st element) when a pathway belongs maximum to one class
"""
maxClass = find(array([max(sum(hcMx[0:j,:],axis=0)) for j in range(1,shape(hcMx)[0])])==1)[-1]+1

#maxClass = find(sum(hcMx[0:j,:],axis=1)>5)[-1]
maxClass = 20

classNames,thcMx,classInt =  ClassesPostProc(hClasses,maxClass=maxClass)
classNames = classNames[0:maxClass]

#because it turns out it's enough to just reverse the order of classInt sorting with 'unique' function

""" Now we have classNames (andthcMx) """

# group mean Effs by class
class_mEffs = [mEffs[find(thcMx[j]==1)] for j in range(maxClass)]
#  and discard NaNs
class_mEffs = [a[~isnan(a)] for a in class_mEffs]

# Take bins as for the whole set of Effs:
temp,bins = np.histogram(allEffs,bins=10,range=[percentile(allEffs,5),percentile(allEffs,95)])
# (percentiles aro to discard some freakishly big values)

if(G_drawAll or 0):
  figure('mean eff distribution per class')
  for j in range(maxClass):
#    subplot(maxClass,1,j+1)
    subplot(4,5,j+1)
    hist(class_mEffs[j],bins,normed=0)
    title(classNames[j],size=10)
    if(j<maxClass-1):
      plt.tick_params(\
          axis='x',          # changes apply to the x-axis
          which='both',      # both major and minor ticks are affected
          bottom='off',      # ticks along the bottom edge are off
          top='off',         # ticks along the top edge are off
          labelbottom='off') # labels along the bottom edge are off
"""
  Here we can see that Metabolism preferers averagely efficient pathways,

  Organismal systems show tendency towards less efficient pathways

  Environmental signal processing - the more efficient ones,

  and Diseases - somewhat in between
    ( actually Diseases distribution looks like
        it's a combination of Organismal systems and Environmental signal processing)

  Although there are actually not so many exmples to trust these distributions too much..
"""


"""  Now the infamous Distributions Distribution s   """

# We take bins as for the whole set of Effs:
temp,bin_edges = np.histogram(allEffs,bins=10,range=[percentile(allEffs,5),percentile(allEffs,95)])
# (percentiles aro to discard some freakishly big values)

bin_centers = [(bin_edges[j]+bin_edges[j-1])/2 for j in range(1,len(bin_edges))]

# nice matrix of densities per bin - densities are normalized to total count
allEffs_distrs = array([np.histogram(a,bins=bin_edges,density=1)[0] for a in Effs])

# I don't like densities, I like portions
allEffs_distrs = array([array(a)/sum(a) for a in allEffs_distrs])

#discard nan values (  where did they come from?? ;)  )
allEffs_distrs[isnan(allEffs_distrs)] = 0;

""" distribution of all distributions: """
if(G_drawAll or 0):
  x = allEffs_distrs.transpose()
  m=array(map(mean,x))
  figure('distribution of all distributions:')
  plot(bin_centers,m)
  pup = array([percentile(a,95) for a in x])
  pdown = array([percentile(a,05) for a in x])
#  plot(bin_centers,m)
#  plot(bin_centers,pup,'--k')
#  plot(bin_centers,pdown,'--k')
  plt.fill_between(bin_centers,pdown,pup,alpha=0.25, antialiased=True)
#  s=array(map(std,x))
#  plot(bin_centers,m+s,'--k')
#  plot(bin_centers,m-s,'--k')
  ylim(0,1)
"""
  Here you can see what portions of reaction efficiencies
    usually fall into each of <nbins> categories

     - mean portion is drawn in a solid line, mean+-std - in dashed

  The downside is that although with stds it looks nice,
    with actual percentages - it does not, because lower 5%,10% and 25%
      - all fall into 0 (so a lot of actual portions of effectiveness are 0)

code to do that would be:
  pup = array([percentile(a,95) for a in x])
  pdown = array([percentile(a,50) for a in x])
  plot(bin_centers,m)
  plot(bin_centers,pup,'--k')
  plot(bin_centers,pdown,'--k')
"""
#aaa
""" distributions of distributions per class """

# take class-specific density vecs
class_Effs_distrs = [allEffs_distrs[find(thcMx[j]==1)] for j in range(maxClass)]
# find means and stds per bin per class
mclass_Effs_distrs  = [array(map(mean,a.transpose())) for a in class_Effs_distrs]
sclass_Effs_distrs  = [array(map(std,a.transpose())) for a in class_Effs_distrs]

class_Effs_pup = [array([percentile(a,95) for a in x.transpose()]) for x in class_Effs_distrs]
class_Effs_pdown = [array([percentile(a,5) for a in x.transpose()]) for x in class_Effs_distrs]


#draw
if(G_drawAll or 0):
  figure('distributions of distributions per class:')
  for j in range(maxClass):
#    subplot(maxClass,1,j+1)
    subplot(4,5,j+1)
    title(classNames[j],size=10)
    plot(bin_centers,mclass_Effs_distrs[j])
#    plot(bin_centers,mclass_Effs_distrs[j]+sclass_Effs_distrs[j],'--k')
#    plot(bin_centers,mclass_Effs_distrs[j]-sclass_Effs_distrs[j],'--k')
#    plt.fill_between(bin_centers,mclass_Effs_distrs[j]-sclass_Effs_distrs[j],mclass_Effs_distrs[j]+sclass_Effs_distrs[j],alpha=0.25, antialiased=True)
#    plot(bin_centers,class_Effs_pup[j],'--k')
#    plot(bin_centers,class_Effs_pdown[j],'--k')
    plt.fill_between(bin_centers,class_Effs_pdown[j],class_Effs_pup[j],alpha=0.25, antialiased=True)
    ylim(0,1)
    if(j<maxClass-1):
      plt.tick_params(\
          axis='x',          # changes apply to the x-axis
          which='both',      # both major and minor ticks are affected
          bottom='off',      # ticks along the bottom edge are off
          top='off',         # ticks along the top edge are off
          labelbottom='off') # labels along the bottom edge are off
"""
  As expected, no particular difference in efficiency distribution patterns

  although noticeable:
   - fast Genetic Information Processing
   - slow Nervous system (even compared to OS in general)
"""

"""
the same but we unite pathways of the classes

 TODO:
   - subj
"""

# we construct class-realted lists of all effs in them
class_Effs_u = []
for cj in range(maxClass):
  temp = []
  [temp.extend(a) for a in Effs[find(thcMx[cj]==1)]]
  class_Effs_u.append(temp)

# nice matrix of densities per bin - densities are normalized to total count
class_Effs_u_distrs = array([np.histogram(a,bins=bin_edges,density=1)[0] for a in class_Effs_u])

# I don't like densities, I like portions
class_Effs_u_distrs = array([array(a)/sum(a) for a in class_Effs_u_distrs])

draw
if(G_drawAll or 0):
  figure('distributions of all effs per class:')
  for j in range(maxClass):
#    subplot(maxClass,1,j+1)
    subplot(4,5,j+1)
    title(classNames[j],size=10)
    splot(bin_centers,class_Effs_u_distrs[j])
    ylim(0,1)
    if(j<maxClass-1):
      plt.tick_params(\
          axis='x',          # changes apply to the x-axis
          which='both',      # both major and minor ticks are affected
          bottom='off',      # ticks along the bottom edge are off
          top='off',         # ticks along the top edge are off
          labelbottom='off') # labels along the bottom edge are off


"""  """

""" clusters' distributions' means distributions """

# group mean Effs by class
cluster_mEffs = [mEffs[find(nzBigClusters==j)] for j in range(max(nzBigClusters)+1)]
#  and discard NaNs
cluster_mEffs = [a[~isnan(a)] for a in class_mEffs]

# Take bins as for the whole set of Effs:
temp,bins = np.histogram(allEffs,bins=10,range=[percentile(allEffs,5),percentile(allEffs,95)])
# (percentiles aro to discard some freakishly big values)

if(G_drawAll or 0):
  figure('mean eff distribution per class')
  for j in range(max(nzBigClusters)+1):
    subplot(max(nzBigClusters)+1,1,j+1)
#    subplot(4,5,j+1)
    hist(cluster_mEffs[j],bins,normed=0)
    title(nzBigClusterNames[j]+' (N='+str(sum(nzBigClusters==j))+')')
    if(j<max(nzBigClusters)+1-1):
      plt.tick_params(\
          axis='x',          # changes apply to the x-axis
          which='both',      # both major and minor ticks are affected
          bottom='off',      # ticks along the bottom edge are off
          top='off',         # ticks along the top edge are off
          labelbottom='off') # labels along the bottom edge are off
"""

  Mean distributions are really similar
  - but they do not actually have to be different

"""



""" clusters' distributions distributions """

# take class-specific density vecs
cluster_Effs_distrs = [allEffs_distrs[find(nzBigClusters==j)] for j in range(max(nzBigClusters)+1)]
# find means and stds per bin per class
mcluster_Effs_distrs  = [array(map(mean,a.transpose())) for a in cluster_Effs_distrs]
scluster_Effs_distrs  = [array(map(std,a.transpose())) for a in cluster_Effs_distrs]

cluster_Effs_pup = [array([percentile(a,95) for a in x.transpose()]) for x in cluster_Effs_distrs]
cluster_Effs_pdown = [array([percentile(a,5) for a in x.transpose()]) for x in cluster_Effs_distrs]

#draw
if(G_drawAll or 0):
  figure('distributions of distributions per cluster:')
  for j in range(max(nzBigClusters)+1):
    subplot(max(nzBigClusters)+1,1,j+1)
#    subplot(4,5,j+1)
    title(nzBigClusterNames[j]+' (N='+str(sum(nzBigClusters==j))+')')
    plot(bin_centers,mcluster_Effs_distrs[j])
#    plot(bin_centers,mclass_Effs_distrs[j]+sclass_Effs_distrs[j],'--k')
#    plot(bin_centers,mclass_Effs_distrs[j]-sclass_Effs_distrs[j],'--k')
#    plt.fill_between(bin_centers,mclass_Effs_distrs[j]-sclass_Effs_distrs[j],mclass_Effs_distrs[j]+sclass_Effs_distrs[j],alpha=0.25, antialiased=True)
#    plot(bin_centers,class_Effs_pup[j],'--k')
#    plot(bin_centers,class_Effs_pdown[j],'--k')
    plt.fill_between(bin_centers,cluster_Effs_pdown[j],cluster_Effs_pup[j],alpha=0.25, antialiased=True)
    if(j<max(nzBigClusters)+1-1):
      plt.tick_params(\
          axis='x',          # changes apply to the x-axis
          which='both',      # both major and minor ticks are affected
          bottom='off',      # ticks along the bottom edge are off
          top='off',         # ticks along the top edge are off
          labelbottom='off') # labels along the bottom edge are off

""" """
#