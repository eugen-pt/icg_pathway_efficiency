# -*- coding: utf-8 -*-
"""
Created on Tue Aug 19 14:57:32 2014

Calculates GO categories overrepresentation on nzClusters

requires nzBigClusters and nzGenes
  (so launch distr_diff_matrices_work.py )
  
  

@author: EP
"""

from binGO_resultsParser import *

try:
  nzBigClusters
  nzGenes
except:
  raise ValueError('launch distr_diff_matrices_work.py  !!')



mode = 'CellComp'
mode = 'BiolProc'
mode = 'MolFun'

try:
  Rs
  if(mode not in Rs.fpath):
    raise ValueError('wrong BinGO ontology mode')
except:
  print('loading BinGO output..')
  Rs = load_BinGO_output(mode)

uc = unique(nzBigClusters)

"""

  bc* = Big Cluster's *
   - parameters per Big Cluster

"""

bcGenes = []
for c in uc:
  temp= []
  [temp.extend(a) for a in array(nzGenes)[array(nzBigClusters)==c]]
  bcGenes.append(temp)

# Over Representation Results
try:
  ORR
  if(len(ORR)!=len(uc)):
    raise ValueError('Different number of clusters in overRep results, need to recalculate')
  if(ORR_mode!=mode):
    raise ValueError('Different mode, need to recalculate')
except:
  print('Testing OverRep..')
  ORR = []
  for j in range(len(bcGenes)):
    print('%i/%i'%(j,len(bcGenes)))
    ORR.append(test_OverRep(Rs,bcGenes[j],Rs.allProtNames))
  ORR_mode = mode  
  print('Done.')

""" filter only significant resutls """

OR_GO_ids = [R.GO_ids[R.GO_FDRps<0.05] for R in ORR]
OR_GO_terms = [R.GO_terms [R.GO_FDRps<0.05] for R in ORR]
OR_GO_FDRps = [R.GO_FDRps [R.GO_FDRps<0.05] for R in ORR]

# All GO ids and terms
aGO_ids = []
[aGO_ids.extend(A) for A in OR_GO_ids]
aGO_ids = unique(aGO_ids)

# reverse_index dictionary - helpful
revIx = {}
for j in range(len(aGO_ids)):
  revIx[aGO_ids[j]] = j

aGO_FDRps = np.zeros((len(aGO_ids),len(OR_GO_ids)))+1
for j in range(len(OR_GO_ids)) :
  for goj in range(len(OR_GO_ids[j])):
#    OR_GO_FDRps[j][goj]
#    OR_GO_ids[j][goj]
#    revIx[OR_GO_ids[j][goj]]
#    aGO_ids[revIx[OR_GO_ids[j][goj]]]
    aGO_FDRps[revIx[OR_GO_ids[j][goj]]][j] = OR_GO_FDRps[j][goj]

# MX - matrix of 1/0 
#  of whether or not the GO catogory is overrepresented in a cluster

MX = np.zeros((len(aGO_ids),len(OR_GO_ids)))


for j in range(len(OR_GO_ids)) :
  for goid in OR_GO_ids[j]:
    MX[revIx[goid]][j]=1

if(1):
  figure('Distribution of number of clusters GO categories are overrepresented in')
  hist(sum(MX,axis=1))

""" 

  So in my case there're a lot of GO categories and most of them
  are only ORed (over-represented) in one cluster.
  
  So anyways - I wanna find these uniquely ORed GO categories

"""

sMX = sum(MX,axis=1)

# Big Clusters' unique GO ids
bcuGO_ids = [aGO_ids[find((sMX==1)*(MX[:,j]))] for j in range(len(bcGenes))]
bcuGO_terms = [ [ Rs.GO_terms[Rs.dictGOIxs_per_id[goid]] for goid in A] for A in bcuGO_ids]
bcuGO_FDRps = [[ aGO_FDRps[revIx[goid]][cj] for goid in bcuGO_ids[cj] ] for cj in range(len(bcuGO_ids))]

with open('output/BigClusters_GO_overrep_'+mode+'.csv','w') as f:
  for cj in range(len(bcGenes)):
    f.write('Cluster,'+str(cj)+','+'\r\n')
    f.write('GO id,FDR p-value,GO term'+'\r\n')
    for j in range(len(bcuGO_ids[cj])):
      f.write(','.join(map(str,[bcuGO_ids[cj][j],bcuGO_FDRps[cj][j], bcuGO_terms[cj][j]]))+'\r\n')

""" """
#

