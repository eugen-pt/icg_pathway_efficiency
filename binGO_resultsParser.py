# -*- coding: utf-8 -*-
"""
Created on Tue Jun 17 14:21:15 2014

parses BinGO output files (*.bgo) via load_BinGO_output(fpath) function

returns entity of BinGO_output class


test overrepresentation via test_OverRep(Rs,genes,background) function

returns entity of BinGO_output class


BinGO_output contains a bunch of stuff, 
all GO-categories related properties are marked by GO_ prefix, 
all protein-related properties are marked - by P_ prefix

list of all genes (.allProtNames), 

list of all GO categories (.GO_ids/.GO_terms)

list of genes/proteins per GO category  (.GO_ProtNames)

list of GO categories per gene/protein (.P_GOids)

list of p-values of overrepresentation per GO category(.GO_ps)


typical usage:

Rs = load_BinGO_output('BinGO\AllGenes_CellComp.bgo') # Loading Background

genes = ['FZD9','NF2','FZD3','ATR','VASP'] # some genes list

R = test_OverRep(Rs,genes,background = Rs.allProtNames) # testing overrepresentation

tix = find(R.GO_FDRps<0.05)
print('Overrepresented categories:')
print('GO ID\tp-value\tp-value corr.\tGO term')
for j in tix:
  print('%i\t%.3g\t%.3g\t%s'%(R.GO_ids[j],R.GO_ps[j],R.GO_FDRps[j],R.GO_terms[j]))

@author: EP
"""
from ep_utilities import *


""" main class """

"""
  suited both for handling BinGO output and 
  my own results of overrepresentation testing

    (since former _is_ latter)
"""
class BinGO_output:
  fpath=''
  # global stuff  
  allProtNames = [] # all protein names (IDs) as listed in BinGO output
  NP=0              # number of unique protein IDs - some may not show here if they do not correspond to GO catogory with p-value <1
  BGTotalGenes = 0  # total number of genes in the background - -||-
  dictGOIxs_per_id = {}
  # GO-related properties, all correspond to GO categories listed in GO_ids and GO_terms
  GO_ids = []
  GO_terms = []
  GO_NHs = []        # Number Here - how many genes/proteins in current list are related to each GO category
  GO_NTs = []        # Number Total - how many g/p in background list are related to this category
  GO_TotalGenes = 0  # total number of genes in tested list - as reported in each line in BinGO output 
#  GO_ProtNames = []  # protein/gene names (IDs) corresponding to each GO category
  GO_ProtIxs = []    # p/g indices in allProtNames array 
  GO_ps = []         # p-values of overrepresentation of each category
  GO_FDRps = []      # FDR corected p-values
  # Protein-related property - ordered as allProtNames
  P_GOids = []       # GO categories per protein/gene
#  P_GOixs = [] # ordered as allProtNames #deprecated
  
""" """
  
# debug stuff  
#fpath = 'BinGO\AllGenes_CellComp.bgo'
#if(1):  

"""
  fucntion to load the BinGO output from .bgo file
"""
def load_BinGO_output(fpath = 'BinGO\AllGenes_CellComp.bgo',verbose=0):
  R = BinGO_output()
  try:
    R.fpath = fpath
    with open(fpath) as f: #'all_s_ProtNames.bgo'
    #with open('all_s_ProtNames_Binomial.bgo')  as f: 
        LS = f.readlines();
  except:
    fpath = 'BinGO\AllGenes_'+fpath+'.bgo'
    R.fpath = fpath
    with open(fpath) as f: #'all_s_ProtNames.bgo'
    #with open('all_s_ProtNames_Binomial.bgo')  as f: 
        LS = f.readlines();
    
    
  LS = LS[find(['GO-ID' in s for s in LS]):]    
  #GO-ID	p-value	corr p-value	x	n	X	N	Description	Genes in test set
  
  LSa = [L[0:-1].split('\t') for L in LS]
  
  header = LSa[0]
#  LSa=array(LSa[1:])
  LSa=LSa[1:]
  
  
  R.GO_ids = array(map(int,[a[0] for a in LSa]))
  R.GO_terms = [a[7] for a in LSa]#LSa[:,7]
  
  R.dictGOIxs_per_id = {}
  for j in range(len(R.GO_ids)):
    R.dictGOIxs_per_id[R.GO_ids[j]] = j
  
  
  R.GO_NHs = map(int,[a[3] for a in LSa])#LSa[:,3])
  R.GO_NTs = map(int,[a[4] for a in LSa])#LSa[:,4])
  R.GO_TotalGenes = int(LSa[0][5])
  R.BGTotalGenes = int(LSa[0][6])
  
  R.GO_ps = array(map(float,[a[1] for a in LSa]))#LSa[:,1]))
  R.GO_FDRps = array(map(float,[a[2] for a in LSa]))#LSa[:,2]))
  if(0):  #if you want to test GO's p-value calcultaion
      if(verbose):
        print('Calculating my p-values')
      
      R.GO_statsPs = R.GO_ps*0.0
      R.GO_statsHPs = R.GO_ps*0.0
      for j in range(len(R.GO_ids)):
          R.GO_statsPs[j] = ep_GOfisher_exact(R.GO_NHs[j],R.GO_NTs[j],R.GO_TotalGenes,R.GO_GOTotalGenes)
      #    R.GO_statsHPs[j] = stats.hypergeom.sf(R.GO_NHs[j],R.GO_TotalGenes,R.GO_NTs[j],1329)
  
  if(verbose):
    print('parsing ProtNames')    
  GO_ProtNames = [s.split('|') for s in [a[8] for a in LSa]]#LSa[:,8]]
  
  if(verbose):
    print('collecting all ProtNames')    
  R.allProtNames = []
##
##   In case total list would be too long for unique function
##      - unique on partial lists (no more than last unique+1000)
##
#  nadd = 0
#  for j in range(len(R.GO_ProtNames)):
#    R.allProtNames.extend(R.GO_ProtNames[j])
#    nadd = nadd+len(R.GO_ProtNames[j])
#    if(nadd>1000):
#      nadd=0;
#      R.allProtNames = unique(R.allProtNames).tolist()
  [R.allProtNames.extend(a) for a in GO_ProtNames]
  if(verbose):
    print('finding unique ProtNames')    
  R.allProtNames = unique(R.allProtNames).tolist()
  
  
  
  R.NP = len(R.allProtNames) # inaccurate measure since not all proteins appear in lists related to significant GO categories
  
  if(verbose):
    print('finding ProtIxs') 
  if(1):   # dictionaries aro so, sooo much faster..
    temp = {}
    for j in range(len(R.allProtNames)) :
      temp[R.allProtNames[j]] = j
    R.GO_ProtIxs = [array( [temp[s] for s in A]) for A in GO_ProtNames]
  else:
    R.GO_ProtIxs = [array( [R.allProtNames.index(s) for s in A]) for A in GO_ProtNames]
  
  if(verbose):
    print('Inverse mapping:  proteins->GOixs')
  new=1
  if(new):
    R.P_GOids = []
    for j in range(len(R.allProtNames)):
      R.P_GOids.append([])

    for goj in range(len(R.GO_ids)):
      for pj in R.GO_ProtIxs[goj]:
        R.P_GOids[pj].append(R.GO_ids[goj])
    
    R.P_GOids = array(R.P_GOids)
  else:
    P_GOixs = [array(find([(j in A) for A in R.GO_ProtIxs])) for j in range(R.NP)];
    R.P_GOids = [[R.GO_ids[j] for j in a] for a in P_GOixs]
  
  # R.GO_PCounts = array(map(len,R.GO_ProtIxs)) #identical to GO_NHs
  return R
  


 

""" 
  Function to test for overrepresented categories in a list of genes/proteins
  
    requires a background BinGO_output to be loaded first
""" 
def test_OverRep(Rs,genes,background=[],verbose=0):
    if(verbose):
      print('testing OverRep..')
    wholeBackGrnd = 0;
    RsBackGrnd = 0
    if(len(background)==0):
      background = Rs.allProtNames;
      wholeBackGrnd = 1
    else:
      if(background==Rs.allProtNames):
        RsBackGrnd = 1
        if(verbose):
          print(' RsBackGrnd!')        
        
    """  wholeBackGrnd  means  that we use the same background as in Rs """
    
    """ 
        I could not think of an easy way to provide option 
        to use Rs.allProtNames as a background so in this case 
        you should provide it manually and all the stuff is calculated 
        just as for any other list of genes 
    """
    
    # looks like that class is perfectly suited to contain results of overrepresetation analysis
    R = BinGO_output()
    

    # we only consider unique genes/proteins which have annotation in Rs
    R.allProtNames = unique(genes)
    tix = find([prot in Rs.allProtNames for prot in R.allProtNames])
    R.allProtNames = R.allProtNames[tix]

    R.GO_TotalGenes = len(R.allProtNames)
    R.NP = len(R.allProtNames)        

    # we obtain annotation        
    R.P_GOids = [Rs.P_GOids[Rs.allProtNames.index(s)] for s in R.allProtNames]
    
    # collect all unique GOids
    R.GO_ids = []
    [R.GO_ids.extend(a) for a in R.P_GOids]
    R.GO_ids = unique(R.GO_ids)
    
    R.GO_terms = array([Rs.GO_terms[Rs.GO_ids.tolist().index(goid)] for goid in R.GO_ids])
    
    R.dictGOIxs_per_id = {}
    for j in range(len(R.GO_ids)):
      R.dictGOIxs_per_id[R.GO_ids[j]] = j
    
    new=1
    if(new):
      R.GO_NHs = np.zeros(len(R.GO_ids))
      
      if(verbose):
        print(' mapping GO_ProtIxs and _ProtNames..')
      
      R.GO_ProtIxs = []
#      R.GO_ProtNames = []
      for j in range(len(R.GO_ids)):
        R.GO_ProtIxs.append([])
#        R.GO_ProtNames.append([])
            
      
      for pj in range(len(R.allProtNames)):
        for goid in R.P_GOids[pj]:
          R.GO_ProtIxs[R.dictGOIxs_per_id[goid]].append(pj)
#          R.GO_ProtNames[R.dictGOIxs_per_id[goid]].append(R.allProtNames[pj])
          R.GO_NHs[R.dictGOIxs_per_id[goid]]=R.GO_NHs[R.dictGOIxs_per_id[goid]]+1
      
      R.GO_ProtIxs = array(R.GO_ProtIxs)
#      R.GO_ProtNames = array(R.GO_ProtNames)
    else:      
    
      if(verbose):
        print(' reverse mapping GO->ProtIxs..')
      # combine reverse array of Proteins (by their indices in the allProtNames array)
      R.GO_ProtIxs = array([ find([goid in a for a in R.P_GOids]) for goid in R.GO_ids])
      if(verbose):
        print(' mapping ProtIxs>ProtNames..')
#      R.GO_ProtNames   = array([ [R.allProtNames[i] for i in a] for a in R.GO_ProtIxs])

    
      # number of GO-related genes in genes list per GOid
      R.GO_NHs = array(map(len,R.GO_ProtIxs))
    
    if(verbose):
      print(' counting NTs..')
    if(wholeBackGrnd):
      R.BGTotalGenes = Rs.BGTotalGenes
      
      temp = list(Rs.GO_ids)
      # total number of GO-related proteins in the background
      R.GO_NTs = array([ Rs.GO_NTs[temp.index(goid)] for goid in R.GO_ids] )


    else:
      BG_allProtNames = unique(background)
      tix = find([prot in Rs.allProtNames for prot in BG_allProtNames])
      BG_allProtNames = BG_allProtNames[tix]
      
      if(RsBackGrnd):
        # total number of GO-related proteins in the background
        R.BGTotalGenes = Rs.GO_TotalGenes
        
        temp = list(Rs.GO_ids)
        R.GO_NTs = array([Rs.GO_NHs[temp.index(goid)] for goid in R.GO_ids])
      else:
        # total number of GO-related proteins in the background
        R.BGTotalGenes = len(BG_allProtNames)
        #full protein lists for our GO categories
        hGO_ProtNames = array(Rs.GO_ProtNames)[ [list(Rs.GO_ids).index(goid) for goid in R.GO_ids]]
        # how many of the background are there      
        R.GO_NTs = array([ sum([p in a for p in BG_allProtNames]) for a in hGO_ProtNames ])
      
#    return R
    if(verbose):
      print(' calculating p-values..')
    R.GO_ps = array([ep_GOfisher_exact(R.GO_NHs[j],R.GO_NTs[j],R.GO_TotalGenes,R.BGTotalGenes) for j in range(len(R.GO_ids))])
    R.GO_FDRps = pFDRadj(R.GO_ps)
    
    tix = argsort(R.GO_ps)
    
    R.GO_FDRps = R.GO_FDRps[tix]
    R.GO_ids = R.GO_ids[tix]
    R.GO_NHs = R.GO_NHs[tix]
    R.GO_NTs = R.GO_NTs[tix]
    R.GO_ProtIxs = R.GO_ProtIxs[tix]  
#    R.GO_ProtNames = R.GO_ProtNames[tix]
    R.GO_ps = R.GO_ps[tix]
    R.GO_terms = R.GO_terms[tix]
    if(verbose):
      print('done.')
#    R.P_GOixs    
    return R
    

""" debug procedure """

"""
  Tests p-values on categories in a bunch of pre-calculated BinGO outputs
"""
#method = 'BiolProc'
#method = 'MolFun'
#method = 'CellComp'

debug=0

try:
  debug
except:
  debug=0

if(debug):
#  for method in ['BiolProc','MolFun','CellComp']:
  for method in ['MolFun','CellComp']:
#  for method in ['CellComp']:
    print(method)
    fpath = 'BinGO\AllGenes_'+method+'.bgo'
    cfpath = 'BinGO_resultsParser_debug\genes_on_AllGenes_'+method+'.bgo'
    
    try:
      Rs
      if(not Rs.fpath==fpath):
        raise ValueError('Reloading Rs from different file')
    except:
      Rs = load_BinGO_output(fpath)    
    with open('BinGO_resultsParser_debug\genes.txt') as f:
      genes = [s[:-1] for s in f.readlines()]
      
    R = test_OverRep(Rs,genes,background = Rs.allProtNames)
    
    Rc = load_BinGO_output(cfpath)
  
    tempL = min([len(R.GO_ids),len(Rc.GO_ids)])
    tablePrintF('%.4g',array([R.GO_ids[0:tempL],R.GO_ps[0:tempL],R.GO_FDRps[0:tempL],Rc.GO_ids[0:tempL],Rc.GO_ps[0:tempL],Rc.GO_FDRps[0:tempL]]).transpose())
    del Rs,R,Rc
#print('Finished')