# -*- coding: utf-8 -*-
"""
Created on Fri Aug  1 10:03:16 2014

imports Names, Classes as Genes for pathways from urls 

requires links.txt to contain all links to pathways 

(are easily obtained from the first column of the xls file)

@author: EP
"""

import urllib2
import re
import pickle
import numpy as np
from numpy import unique,array


try:
  with open('output\\LinksNamesClassesL.pickle') as f:
    LinksDone,Names,Classes,Genes,Ls = pickle.load(f)
  if(len(np.unique(map(len,[LinksDone,Names,Classes,Genes])))>1):
      raise ValueError('Different length of read arrays!')
    
except:    

  WTF  #had some difficulties and didnt wanna wait until all 281 links are parsed
  # So i created this error indicator
  #   (it needs to be removed if you want to re-parse)
  
  with open('src\\links.txt') as f:
    Links = f.readlines()
  
  Links = [s[0:s.index('/kgml')] for s in Links]
  
  Ls = []
  
  try:
    with open('output\\LinksDone.txt') as f:
      LinksDone = [s[0:-2] for s in f.readlines()]
    with open('output\\Names.txt') as f:
      Names = [s[0:-2] for s in f.readlines()]
    with open('output\\Classes.txt') as f:
      Classes = [s[0:-2].split(';') for s in f.readlines()]
    with open('output\\Genes.txt') as f:
      Genes = [s[0:-2].split(';') for s in f.readlines()]  
    if(len(unique(map(len,[LinksDone,Names,Classes,Genes])))>1):
        raise ValueError('Different length of read arrays!')
  except:  
    LinksDone = []
    Names = []
    Classes = []
    Genes = []
    with open('output\\LinksDone.txt','w') as f:
      pass
    with open('output\\Names.txt','w') as f:
      pass
    with open('output\\Classes.txt','w') as f:
      pass
    with open('output\\Genes.txt','w') as f:
      pass
  
  import threading
  
  
  for j in range(len(Links)):
    
    url = Links[j]  
  
    if(url in LinksDone):
      continue
  
  
    print('%i/%i..'%(j,len(Links)))
    
    # Loading
    R=urllib2.urlopen(url)
    rr=R.read()
    L=rr.split('\n')
    
  #  Ls.append(L) #for debug purposes
  
    name = L[1][4:L[1].index(' - ')].strip()
    Names.append(name)
  
    # Classes - we search for a line with 'CLASS' in it (for all of them actually)
    classes = []
    for l in L:
      try:
        classes.extend([s.strip() for s in l[(l.index('CLASS')+5):].strip().split(';')])
      except:
        pass
    Classes.append(classes)
    # Genes - 
    genes=[]
    for j in range(len(L)):
        if('GENE' in L[j]):
            for i in range(j,len(L)):
                if(('COMPOUND' in L[i]) or ('REFERENCE' in L[i])):
                    break
                temp = re.findall('^(GENE)? +[0-9]+ +([A-Za-z0-9]+);',L[i])
                try:
                  genes.append(temp[0][1])
                except:
                  print('Some issue in:\nurl='+url+'\nline='+str(i))
                        
            break
    Genes.append(genes)    
  
    LinksDone.append(url)
    with open('output\\LinksDone.txt','a') as f:
      f.write(url+'\r\n')
    with open('output\\Names.txt','a') as f:
      f.write(name+'\r\n')
    with open('output\\Classes.txt','a') as f:
      f.write(';'.join(classes)+'\r\n')
    with open('output\\Genes.txt','a') as f:
      f.write(';'.join(genes)+'\r\n')
  
  
  # Pickle dump..
  with open('output\\LinksNamesClassesL.pickle','w') as f:
    pickle.dump((LinksDone,Names,Classes,Genes,Ls),f)  
 
 
# Matrix of classes 
 
def ClassesPostProc(Classes,maxClass=-1):
  
#if(1):
  allClasses = [] 
  allClassNames = []
  for c in Classes:
    try:
      allClasses.append(c[0])  # append major classes
      allClassNames.append(c[0])  # append major classes
      if(len(c)>1):
        allClasses.append(c[1])
        temp = c[0]
        temp = temp.replace('Metabolism','M->')
        temp = temp.replace('Organismal Systems','OS->')
        temp = temp.replace('Human Diseases','HD->')
        temp = temp.replace('Cellular Processes','CP->')
        temp = temp.replace('Genetic Information Processing','GIP->')
        temp = temp.replace('Environmental Information Processing','EIP->')
        allClassNames.append(temp+c[1])
    except:
      pass
        
  uaCN,tix = np.unique(allClassNames,return_index=1)
  uaC  = [allClasses[j] for j in tix]

  Mx = []
  for c in uaC:
    Mx.append([c in A for A in Classes])  

  sMx = map(sum,Mx)
  
  tix = np.argsort(sMx).tolist()
  tix.reverse()
  
  uaC = np.array(uaC)
  uaCN = np.array(uaCN)
  Mx = array(Mx)
  
  uaC = uaC[tix]
  uaCN = uaCN[tix]
  Mx = Mx[tix,:]
  
  cNames = uaCN
  cMx = Mx
  
  #  Calculating smart stuff 
  # - special Integer, made as an array to sort and get indices so that huge arrays would be 'condenced'

  classMx = Mx
  
  classInt = np.zeros(shape=(len(Classes)))
    
  if(maxClass<=0):  
    maxClass = max(np.where(np.sum(Mx,axis=1)>5)[0])
    
  tempMul = 2**(maxClass -array(range(maxClass)))
  
  for j in range(len(Classes)):
      classInt[j] = sum(Mx[0:maxClass,j]*tempMul)
  cInt = classInt
  return cNames,cMx, cInt   

uaC,cMx,cInt = ClassesPostProc(Classes)
  
""" Saving the matrix..  """

save=0  
if(save):
  with open('output\\Classes_table.csv','w') as f:
    f.write('links,')
    for c in uaC:
      f.write(c+',')
    f.write('\n')
    for j in range(len(LinksDone)):
      l=LinksDone[j]
      f.write(l+',')
      for c in uaC:
        f.write(str(int(c in Classes[j]))+',')
      f.write('\n')

"""  """
 
""" Sorting the Links and Stuff: """

tix = np.argsort(LinksDone)
LinksDone = np.array(LinksDone)[tix]
hsaIDs = [s[s.index(':hsa')+4:] for s in LinksDone]
Names = np.array(Names)[tix]
Classes = np.array(Classes)[tix]
Genes = np.array(Genes)[tix]


""" """ 
#